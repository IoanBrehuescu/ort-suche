const ortInput = document.querySelector('#ort');
const plzInput = document.querySelector('#plz');
const longitudeInput = document.querySelector('#longitude');
const latitudeInput = document.querySelector('#latitude');
const ortVorschlaegeDiv = document.querySelector('#ortVorschlaege');

let debounceTimeout;
ortVorschlaegeDiv.style.display='none'
ortInput.addEventListener('input', () => {
    
    clearTimeout(debounceTimeout);
    const ort = ortInput.value;
    
    debounceTimeout = setTimeout(() => {
        if (ort.length > 2) {

            ladeOrte(ort);
            ortVorschlaegeDiv.style.display='block'
            
        } 
        else {
            
            ortVorschlaegeDiv.innerHTML = '';
        }
    }, 300);
});

function zeigeProgressBar() {
    document.querySelector('#progressBarContainer').style.display = 'block';
    document.querySelector('#progressBar').style.width = '50%';
}

function verbergeProgressBar() {
    document.querySelector('#progressBar').style.width = '100%';
    setTimeout(() => {
        document.querySelector('#progressBarContainer').style.display = 'none';
        document.querySelector('#progressBar').style.width = '0';
    }, 500);
}
        

function ladeOrte(ort) {
    
    zeigeProgressBar();
    const req = new XMLHttpRequest();
    req.open('GET', `http://wifi.1av.at/getplz.php?json&ort=${ort}`);
    req.onload = () => {
        if (req.readyState === 4) {
            const jsonResponse = JSON.parse(req.responseText);
            // Clear previous suggestions
            
            ortVorschlaegeDiv.innerHTML = '';
            
            Object.keys(jsonResponse).forEach(plz => {
                jsonResponse[plz].forEach(city => {
                    if (city.toLowerCase().includes(ort.toLowerCase())) {
                        const div = document.createElement('div');
                        div.textContent = `${city} (${plz})`;
                        div.className = 'ort-vorschlag'; // Assign class
                        div.onclick = () => waehleOrt(city, plz);
                        ortVorschlaegeDiv.appendChild(div);


                       
                    }
                });
            });

            
        }
        verbergeProgressBar();
    };
    req.send();
}

// Funktion, die aufgerufen wird, wenn ein Ort ausgewählt wurde
function waehleOrt(city, plz) {
    ortInput.value = city;
    plzInput.value = plz;
    ortVorschlaegeDiv.innerHTML = '';
    ortVorschlaegeDiv.style.display = 'none';
    verbergeProgressBar()
    ladeKoordinaten(plz);
}


// Funktion zum Laden der Koordinaten
function ladeKoordinaten(plz) {
    zeigeProgressBar();
    const req = new XMLHttpRequest();
    req.open('GET', `http://api.zippopotam.us/AT/${plz}`);
    req.onload = () => {
        if (req.status === 200 && req.readyState === 4) {
            const response = JSON.parse(req.responseText);
            const place = response.places[0];
            longitudeInput.value = place.longitude;
            latitudeInput.value = place.latitude;
            verbergeProgressBar();
        }
    };
    req.send();
}
